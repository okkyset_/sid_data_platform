import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from '../../pages/Landingpage/Home'
import Login from '../../pages/Landingpage/Login'
import ForgotPassword from '../../pages/Landingpage/ForgotPassword'
import Signup from '../../pages/Landingpage/Signup'
import Pricing from '../../pages/Landingpage/Pricing'
import Database from '../../pages/Dashboard/Database'
import Requestconnect from '../../pages/Dashboard/Requestconnect'
import SidReport from '../../pages/Dashboard/SidReport'
import Notifpopupupgrade from '../../pages/Landingpage/Notifpopupupgrade'
import Dashboard from '../../pages/Dashboard/Dashboard'

const Routes = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Home></Home>
                </Route>
                <Route path="/login">
                    <Login></Login>
                </Route>
                <Route path="/forgotpassword">
                    <ForgotPassword></ForgotPassword>
                </Route>
                <Route path="/signup">
                    <Signup></Signup>
                </Route>
                <Route path="/pricing">
                    <Pricing></Pricing>
                </Route>
                <Route path="/database">
                    <Database></Database>
                </Route>
                <Route path="/requestconnect">
                    <Requestconnect></Requestconnect>
                </Route>
                <Route path="/sidreport">
                    <SidReport></SidReport>
                </Route>
                <Route path="/notifpopupupgrade">
                    <Notifpopupupgrade></Notifpopupupgrade>
                </Route>
                <Route path="/dashboard">
                    <Dashboard></Dashboard>
                </Route>
            </Switch>
        </Router>
    )
}
export default Routes