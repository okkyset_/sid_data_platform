import React, { Component } from 'react';
import SidebarMenu from '../../components/Dashboard/SidebarMenu';
import ContentDataBase from '../../components/Dashboard/ContentDataBase';
import SidebarTop from '../../components/Dashboard/SidebarTop';

class Database extends Component {
    componentDidMount() {

    }
    render() {

        return (
            <div>
                <SidebarMenu></SidebarMenu>
                <SidebarTop textmenupage="Database"></SidebarTop>
                <ContentDataBase></ContentDataBase>
            </div>
        )
    }
}
export default Database