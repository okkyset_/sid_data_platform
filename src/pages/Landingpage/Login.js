import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../images/logosid.png'
import Logogoogle from '../../images/google.jpg'
import $ from 'jquery';
class Login extends Component {
    componentDidMount() {
        $(".show-password, .hide-password").on('click', function() {
		    var passwordId = $(".ipassword input");
		    if ($(this).hasClass('show-password')) {
		      $(passwordId).attr("type", "text");
		      $(this).parent().find(".show-password").hide();
		      $(this).parent().find(".hide-password").show();
		    } else {
		      $(passwordId).attr("type", "password");
		      $(this).parent().find(".hide-password").hide();
		      $(this).parent().find(".show-password").show();
		    }
		});
    }
    render() {

        return (
            <div>
                <div className="bg-login-page">
                    {/* <img src="images/bglogin.png" alt="" /> */}
                </div>
                <div className="dont-have-account">
                    <h3 className="title-donthaveaccount">Don't have account ? </h3>
                    <Link to="/Signup" className="bt-signup-log">Signup</Link>
                </div>
                
                <div className="wrap-login-page">
                    <div className="wrap-login-page-in">
                        <div className="box-login-form">
                            <div className="heading-title-login-form">
                                Login to <img src={Logo} alt=""/>
                            </div>
                            <div className="in-box-login-form">
                                {/* <!-- email --> */}
                                <div className="item-form-login-i">
                                    <h4 className="form-input-login-desc">Email <span>*</span></h4>
                                    <div className="i-input-form-login iemail">
                                        <input type="text" placeholder="Enter Email"/>
                                    </div>
                                </div>
                                {/* <!-- password --> */}
                                <div className="item-form-login-i">
                                    <h4 className="form-input-login-desc">Choose Password <span>*</span></h4>
                                    <div className="i-input-form-login ipassword">
                                        <input type="password" placeholder="Enter Password"/>
                                        <div className="bt-show-password-c">
                                            <span className="show-password">Show</span>
                                            <span className="hide-password">Hide</span>
                                        </div>
                                    </div>
                                    <div className="cta-forgot-password">
                                        <Link to="/forgotpassword">Forgot Password ?</Link>
                                    </div>
                                </div>
                                {/* <!-- login --> */}
                                <div className="item-form-login-i">
                                    <input type="submit" value="Login" className="submit-login-f"/>
                                </div>
                                <div className="item-form-login-i">
                                    <h3 className="text-or">OR</h3>
                                </div>
                                <div className="item-form-login-i">
                                    <Link to="#" className="bt-login-with-google">
                                        <img src={Logogoogle} alt=""/> Continue with Google
                                    </Link>
                                </div>
                                <div className="item-form-login-i">
                                    <h3 className="text-cant-login">
                                        <Link to="/forgotpassword">Can't Login?</Link>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        )
    }
}
export default Login