import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Sidreport from '../../images/sidreport.png'
import logosid from '../../images/lgsid.png'
import NotifUpgradePro from './NotifUpgradePro';

class ContentSidReport extends Component {
    render() {
        return (
            <div>
                <div className="wrap-dashboard-content-section-page">
                    <div className="wrap-dashboard-content-section-page-in">
                        {/* pro plan section */}
                        {/* <NotifUpgradePro></NotifUpgradePro> */}
                        {/* tab table */}
                        <div className="wrap-content-tab-database-table">
                            <h3 className="heading-title-sid-report-page">
                                StartupIndonesia Report
                            </h3>
                            <div className="wrap-item-download-sid-report">
                                {/* item */}
                                <div className="item-download-sid-report-download">
                                    <div className="item-download-sid-report-download-in">
                                        <div className="item-download-sid-report-download-img">
                                            <img src={Sidreport} alt=""/>
                                        </div>
                                        <div className="item-download-sid-report-download-desc">
                                            <h3 className="title-item-report-down-desc">
                                                StartupIndonesia Top 15 Investability Points - Q1 2021
                                            </h3>
                                            <div className="item-download-sid-report-meta">
                                                <img src={logosid} alt=""/>
                                                <span className="item-do-meta-sid-report-by">
                                                    StartupIndonesia
                                                </span>
                                                <div className="meta-time-sid-report">
                                                    12h ago
                                                </div>
                                            </div>
                                            <Link to="#" className="bt-download-sid-report-item-cta">Download</Link>
                                        </div>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                                {/* item */}
                                <div className="item-download-sid-report-download">
                                    <div className="item-download-sid-report-download-in">
                                        <div className="item-download-sid-report-download-img">
                                            <img src={Sidreport} alt=""/>
                                        </div>
                                        <div className="item-download-sid-report-download-desc">
                                            <h3 className="title-item-report-down-desc">
                                                StartupIndonesia Top 15 Investability Points - Q1 2021
                                            </h3>
                                            <div className="item-download-sid-report-meta">
                                                <img src={logosid} alt=""/>
                                                <span className="item-do-meta-sid-report-by">
                                                    StartupIndonesia
                                                </span>
                                                <div className="meta-time-sid-report">
                                                    12h ago
                                                </div>
                                            </div>
                                            <Link to="#" className="bt-download-sid-report-item-cta">Download</Link>
                                        </div>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default ContentSidReport