import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import parse from 'html-react-parser';
import NotifUpgradePro from './NotifUpgradePro';

class ContentRequestconnect extends Component {
    render() {
        const iframe = parse('<iframe class="airtable-embed" src="https://airtable.com/embed/shrmvkSkyxf8DMpTW?backgroundColor=grayLight&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>');
        return (
            <div>
                <div className="wrap-dashboard-content-section-page">
                    <div className="wrap-dashboard-content-section-page-in">
                        {/* <NotifUpgradePro></NotifUpgradePro> */}
                        
                        {/* tab table */}
                        <div className="wrap-content-tab-database-table">
                            <div className="wrap-tab-database-data-table wrap-tab-database-data-table-iframe">
                                {iframe}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default ContentRequestconnect