import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Tab from 'react-bootstrap/Tab'
import Nav from 'react-bootstrap/Nav'
import parse from 'html-react-parser';
import NotifUpgradePro from './NotifUpgradePro';

class ContentDataBase extends Component {
    render() {
        const iframe = parse('<iframe class="airtable-embed" src="https://airtable.com/embed/shrYgNDc27QMC1kFg?backgroundColor=grayLight&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>');
        const iframe2 = parse('<iframe class="airtable-embed" src="https://airtable.com/embed/shruwLDT1frO8EPIh?backgroundColor=grayLight&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>');
        return (
            <div>
                <div className="wrap-dashboard-content-section-page">
                    <div className="wrap-dashboard-content-section-page-in">
                        {/* pro plan section */}
                        <NotifUpgradePro></NotifUpgradePro>
                        {/* tab table */}
                        {/* <div className="wrap-content-tab-database-table">
                            <Tab.Container id="tabs-our-solution" defaultActiveKey="tab1">
                                <Nav variant="pills" className="flex-column">
                                    <Nav.Item className="nav-item-tab-database-tab">
                                        <Nav.Link eventKey="tab1">Startup Database</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item className="nav-item-tab-database-tab">
                                        <Nav.Link eventKey="tab2">Scoring Recap</Nav.Link>
                                    </Nav.Item>
                                    <div className="clear"></div>
                                </Nav>
                                <Tab.Content>
                                    <Tab.Pane eventKey="tab1">
                                        <div className="wrap-tab-database-data-table wrap-tab-database-data-table-iframe">
                                            {iframe}
                                        </div>
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="tab2">
                                        <div className="wrap-tab-database-data-table wrap-tab-database-data-table-iframe">
                                            {iframe2}
                                        </div>
                                    </Tab.Pane>
                                </Tab.Content>
                            </Tab.Container>
                        </div> */}
                    </div>
                </div>
            </div>
        )
    }

}
export default ContentDataBase