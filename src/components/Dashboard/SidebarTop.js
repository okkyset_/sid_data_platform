import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import avatar from '../../images/avatar1.png'

class SidebarTop extends Component {
    constructor(props) {
        super(props)
        this._isMounted = false;
    }
    render() {
        return (
            <div>
                <div className="wrap-sidebar-top-dashboard-menu-page">
                    <div className="wrap-sidebar-top-dashboard-menu-page-in">
                        <div className="bar-menu-toogle-show-responsive">
                            <i className="fa fa-bars"></i>
                        </div>
                        <h3 className="title-dashboard-meta-top-sidebar">
                            {this.props.textmenupage}
                        </h3>
                        <div className="profile-section-top-bar-dashboard">
                            {/* <div className="img-profile-user-dashboard-table">
                                <img src={avatar} alt=""/>
                            </div> */}
                            <div className="user-name-profile-dasboard-table">
                               Hi, anonymous
                            </div>
                            <Link className="signout-bt-dash"><i className="fa fa-power-off"></i> <span class="text-signout-bt">Sign Out</span></Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default SidebarTop