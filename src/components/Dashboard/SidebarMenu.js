import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Logo from '../../images/logo.png'
import prover from '../../images/prover.png'
import $ from 'jquery';

class SidebarMenu extends Component {
    componentDidMount() {
        $('.bar-menu-toogle-show-responsive').on("click", function (e) {
            e.preventDefault();
            $(".wrap-dashboard-sidebar-main-menu").addClass("wrap-dashboard-sidebar-main-menu-active");
            $(".bt-close-sidebar-menu-dashboard").addClass("bt-close-sidebar-menu-dashboard-active");
            $("body").addClass("body-menu-dasboard-active");
        });
        $('.bt-close-sidebar-menu-dashboard').on("click", function (e) {
            e.preventDefault();
            $(".wrap-dashboard-sidebar-main-menu").removeClass("wrap-dashboard-sidebar-main-menu-active");
            $(".bt-close-sidebar-menu-dashboard").removeClass("bt-close-sidebar-menu-dashboard-active");
            $("body").removeClass("body-menu-dasboard-active");
        });
        $('.main-menu-sidebar-dashboard-database li a').on("click", function () {
            $("body").removeClass("body-menu-dasboard-active");
        });
    }
    render() {
        
        const routes = window.location.pathname
        return (
            <div>
                {/* close menu mobile */}
                <div className="bt-close-sidebar-menu-dashboard">
                    <i className="fa fa-close"></i>
                </div>
                <div className="wrap-dashboard-sidebar-main-menu">
                    <div className="main-logo-dashboard-sidebar-menu">
                        <img src={Logo} alt=""/>
                    </div>
                    <div className="wrap-main-menu-sidebar-dashboard-database">
                        <ul className="main-menu-sidebar-dashboard-database">
                            <li class={routes == "/dashboard"?"active":""}>
                                <Link to="/dashboard"><i className="fa fa-line-chart"></i> Dashboard</Link>
                            </li>
                            <li class={routes == "/database"?"active":""}>
                                <Link to="/database"><i className="fa fa-database"></i> Database</Link>
                            </li>
                            <li class={routes == "/sidreport"?"active":""}>
                                <Link to="/sidreport"><i className="fa fa-file-text"></i> SID Report</Link>
                            </li>
                            <li class={routes == "/requestconnect"?"active":""}>
                                <Link to="/requestconnect"><i className="fa fa-phone"></i> Request Connect</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="box-upgrade-to-pro-ver">
                        <div className="img-icon-upgrade-to-pro-v">
                            <img src={prover} alt=""/>
                        </div>
                        <h3 className="title-upgrade-to-pro-v">
                            Upgrade to <span>PRO</span> for more feature
                        </h3>
                        <Link to="#" className="bt-upgrade-to-pro-version">Upgrade</Link>
                    </div>
                </div>
            </div>
        )
    }

}
export default SidebarMenu